#!/bin/bash
BASEPATH="$(dirname "$(readlink -f "$0")")"
echo "$BASEPATH"
cd "$BASEPATH"
pacman -S opus ortp

cd trx-0.5
make
cd ..

ln -sf "$BASEPATH/tix.service" /etc/systemd/system/
ln -sf "$BASEPATH/rix.service" /etc/systemd/system/
ln -sf "$BASEPATH/tix" /usr/local/bin/
ln -sf "$BASEPATH/rix" /usr/local/bin/

systemctl daemon-reload

FILE=/boot/starter.txt
if test -f "$FILE"; then
echo "## [trix] audio stream TX/RX
# tix   # transmit (to gateway)
# rix   # receive 
" >> /boot/starter.txt
fi